#include "interactive.h"


int Interactive::start() {
	loop = true;
	std::cout << "Welcome to NumToLetterBaseConverter!  ['w']\nthe amazingly \t\t\t      <(V)>\n\tconcisely named \t       / \\\n\t\tTable Header Calculator!\nType \"Exit\" to exit the program at any time\n------------------------------------------\n";
	while (loop) {
		parseCommands(getInput("Input an integer to convert"));
		/*
		//gets a user provided value, converts it, and prints it.
		std::cout << converter.convert(getInputInt("Input an integer to convert")) << std::endl;

		//ask if the user would like to exit
		std::string input = getInput("Exit? (y/n)");
		if (input[0] == 'y' || input[0] =='Y') {
			loop = false;
		}
		*/
	}

	return 0;
}

std::string Interactive::getInput(std::string prompt) {
	std::cout << prompt << std::endl << ">>";//helps user notice they are being asked to do something
	std::string input;
	std::getline(std::cin, input);
	return input;
}

int Interactive::parseCommands(std::string input) {
	//will check the input against commands until it finds something, then it will immediately react.

	//lowercase the input
	for (auto it = input.begin(); it != input.end(); ++it) {
		if (*it >= 'A' && *it <= 'Z') {//if uppercase
			*it += 'a' - 'A'; //set it to lowercase. ascii's probably not changing soon could probably put 32 here.
		}
	}

	for (int i = 0; i < commandList.size(); ++i) {
		if (std::regex_match(input, commandList[i].second)) {
			handleCommand(i, input);
			return i;
		}
	}
	std::cout << "Sorry, did not catch that. \nAcceptable commands are integers or Exit. ['w']\n";
	return -1;
}

void Interactive::handleCommand(int commandFlag, std::string message) {
	//TODO  right now if commandList is reordered, this function will break
	//maybe the string could be hashed, but that is a sketchy and more expensive than necessary.
	//the switch could be converted to an if else chain, but this is nonstandard for a handler '\_('w')_/`
	switch (commandFlag){  //this->commandList.at(commandFlag)->first) { //this does not work as switch statements evidently require integral arguments in c++
	case 0: //exit
		commandExit();
		break;

	case 1: //number
		commandNumber(message);
		break;
	}
}

void Interactive::commandExit() {
	std::cout << "See ya!\n";
	loop = false;
}

void Interactive::commandNumber(std::string input) {
	std::string output = "1"; //defaults to a number because output should be "-?[A-Z]*" if it succeeds.

	//translate the input to a number
	//if there are exceptions, they will be arrising from stoi().
	try {
		output = converter.convert(stoi(input));
	}
	catch (std::invalid_argument e) {
		std::cout << "Sorry, that value was invalid.\nTry pressing just numbers next time ['w']\n";
	}
	catch (std::out_of_range e) {
		std::cout << "Sorry, that value was too extreme for an integer. ['w']\n";
	}


	if (output != "1") { //if we have some sort of output;
		std::cout << output << "\n\n";
	}
}