#include "commandLine.h"

CommandLine::CommandLine(int & argc, char* argv[]) {
	this->argc = argc;
	this->argv = argv;
}

CommandLine::~CommandLine() {
	argv = NULL;//not really necessary since this value wasn't NEW.
}


int CommandLine::start() {

	//goes through the list of arguments and translates them all into alphabet column headers.
	//illegal arguments are treated as 0.
	for (int i = 1; i < argc; ++i) {
		std::cout << converter.convert(std::atoi(argv[i]));
		if (i != argc - 1) {
			std::cout << ' ';
		}
	}
	return 0;
}