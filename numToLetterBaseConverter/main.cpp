#include "runMode.h"
#include "interactive.h"
#include "commandLine.h"

int main(int argc, char *argv[])
{
	RunMode * mode;

	//has 2 runModes
	//	interactive, if there are no command line arguments
	//		will prompt and walk user through
	//	commandline, if there are command line arguments
	//		place values to be translated as command line arguments and they will all be translated.
	if (argc > 1) {
		mode = new CommandLine(argc, argv);
	}
	else {
		mode = new Interactive();
	}

	//start the program.
	int output = mode->start();

	//finish
	delete mode;
	return output;
}

