#pragma once

#include <iostream>

#include "numberToLetterConverter.h"
#include "runMode.h"

class CommandLine : public RunMode {
public:
	CommandLine(int & argc, char* argv[]);

	~CommandLine();

	int start();

private:
	int argc;
	char** argv;
};