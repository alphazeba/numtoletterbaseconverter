#pragma once
#include "numberToLetterConverter.h"

class RunMode {
public:
	virtual int start() = 0;

protected:
	NumberToLetterConverter converter;
};