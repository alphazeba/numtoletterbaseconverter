#pragma once

#include <vector>
#include <string>

class NumberToLetterConverter {
public:
	//converts the given int into base 26 alphabet representation.
	// 0 = "",  1= "A" , 26 = "Z", 27="AA" , -1="-A"
	std::string convert(int n);

private:
	//returns quotient, takes a variable reference to also output the remainder.
	int divideQR(const int& dividend, const int& divisor, int& OUTremainder);
};