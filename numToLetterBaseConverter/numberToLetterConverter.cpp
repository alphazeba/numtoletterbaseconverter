
#include "numberToLetterConverter.h"

std::string NumberToLetterConverter::convert(int n) {
	//this divides the input value by 26 and records the remainders to find the base 26 representation of the number.

	//normally the min symbol value = 0
	//in this case the min symbol value (A) = 1
	//therefore there is a hole ( ..."-A","","A",...) in the middle of the number line
	if (n == 0) {
		return "";
	}

	//handle negative values.
	std::string output = "";
	if (n < 0) { // if the value is negative.
		n *= -1; //flip it positive
		output += '-';  //add negative prefix to the output.
	}

	//find the remainders.
	std::vector<short> remainders;
	int r; //this will hold the remainder during the while loop.
	while (--n >= 26) {  //to adjust for A=1 and the hole resulting from no 0, 1 is subtracted each time. notice 1 is subtracted even when the while loop fails.
		n = divideQR(n, 26, r);//divides n by 26 and also saves the remainder in r.
		remainders.push_back((short)r);
	}
	//don't need to divide on the last loop since n is less than 26 and the entirety of it will become the remainder.
	remainders.push_back((short)n);
	
	//reverse remainders and translate the remainders into char.
	for (auto it = remainders.rbegin(); it != remainders.rend(); ++it) {
		output += (char)(*it) + 'A';
	}

	return output;
}

int NumberToLetterConverter::divideQR(const int& dividend, const int& divisor, int& OUTremainder) {
	OUTremainder = dividend % divisor;
	return dividend / divisor;
}