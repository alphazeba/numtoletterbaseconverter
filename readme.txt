
This program converts integers into A,B,C,...,Z,AA,AB,... format.

it has an interactive and commandline argument modes.

Interactive mode:
	To boot into interactive mode, start the program without any arguments.  
	In interactive mode, input integers and the program will convert them.  
	Type "Exit" when you are done.

Commandline mode:
	To boot into commandline mode, start the program with 1 or more integers provided as arguments.  
	The program will convert these and output them as a space seperated list.