#pragma once


#include <iostream>
#include <string>
#include <regex>
#include <locale>

#include "numberToLetterConverter.h"
#include "runMode.h"

typedef std::vector<std::pair<std::string, std::regex>> CommandList;
typedef std::pair<std::string, std::regex> Command;

class Interactive : public RunMode{
public:
	int start();

private:
	//interactive mode continues as long as loop is true.
	bool loop;

	//gets unverified input from the user.
	std::string getInput(std::string prompt);

	//checks if input is a command, if it is returns the command number and triggers the command handler.
	int parseCommands(std::string input);

	void handleCommand(int commandFlag, std::string message);

	  ////////////
	 //commands//
	////////////
	//command list
		//when commands are parsed they will be set to lower prior.
		//A Command is a std::pair consisting of std::string first and std::regex second
	CommandList commandList = {
		Command("exit","^exit"), //exit program
		Command("number","^-?\\d+") //a number.  this allows for any length positive or negative number.
	};

	//command handlers.
	void commandExit();
	void commandNumber(std::string input);
};


